package com.group.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by carlos on 16/09/16.
 */
@Repository
public class DataDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Map<String, Object>> getData() {
        List<Map<String, Object>> values = jdbcTemplate.queryForList("SELECT LAT LAT,LON LON,COUNT(0) COUNT FROM PROYECTO.HISTORICO_RESUMEN_3 GROUP BY AGENCIA");
        return values;
    }
}
